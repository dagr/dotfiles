#!/bin/sh

~/.fehbg

dunst &

dt() {
        printf "%s" "$(date +"%A, %B %d - %H:%M" | tr -d '\n' | tr '[:upper:]' '[:lower:]')"
}

eth() {
        printf "eth %s" "$(cat /sys/class/net/e*/operstate | tr -d '\n')"
}

wifi() {
        printf "wifi %s" "$(cat /sys/class/net/w*/operstate | tr -d '\n')"
}

vol() {
	printf "volume %s" "$(amixer get Master | tail -1 | awk '{ print $5 }' | tr -d '[]')"
}

while true; do
        xsetroot -name " $(vol) | $(wifi) | $(eth) | $(dt)"
        sleep 30s
done &
